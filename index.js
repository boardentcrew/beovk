var crypto = require('crypto');
var bespeke = require('../beSpeke/index.js');
var bs58 = require('base-x')('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');

function buildKeys() {
	var seed = crypto.randomBytes(32);
	var root = seed;
	var rainbow=[];

	for(i=0;i<256;i++) rainbow[i] = crypto.createHash('sha256').update(root).digest();

	var me = new bespeke(rainbow[254]);
	var prefix = rainbow[255];
	var key = me.getPublicKey();

	var badge =  Buffer.concat([ prefix,key  ]);
	var address = crypto.createHash('sha256').update(badge).digest();

	var signature = me.sign('I am real human boi!');
	

	// Decrypt this with my public key which is ... -- 

	console.log('Seed : ',seed.toString('hex'));
	console.log('Iter 1 : ',rainbow[255].toString('hex'));
	console.log('Address : ',bs58.encode(address) );
	console.log('Badge : ',bs58.encode( badge ) );
	console.log('Signature : ',bs58.encode(signature));

	if (me.verify('I am real human boi!',signature)) {
		console.log('Signature verified!');
	} else {
		console.log('Signature unverifiable.');
	}

}

//buildRoot();
buildKeys();
