var crypto = require('crypto');
var bs58 = require('base-x')('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');

function buildRoot() {
	const buf = crypto.randomBytes(128);
	return buf;
}

// A public key must be present, the private key should be held only by those who can write the passport.
// A user can 'molt' an identity, which chooses a new keypair and verifiable iteration of the previous passport, only iterationable by the seed.
// A identity chain can be 256 identities long.
// Process of making an identity chain

// Passport has data object seperate from header obj
// Header obj is as such
// -- Address
// -- Badge
// -- KeyRing
// -- -- -- Badge ( molt 1 )
// -- -- -- Badge ( molt 2 )
// -- -- -- Badge ( molt 3 )
// -- -- -- Badge ( molt 4 - final; use this pubkey )
//
// The main address never changes, but each new molted identity changes the key pair
// 

function sign(pair,data) {
	var pairB = crypto.createDiffieHellman( 256 );
	pairB.generateKeys();
	var hash = crypto.createHash('sha256').update( data ).digest();
	pairB.setPrivateKey( hash );
	var pubb = pairB.getPublicKey();
	var secret = pair.computeSecret( pubb );
	var publicKey = pair.getPublicKey();
	return { 
		'hash':hash,
		'secret':secret,
		'publicKey':publicKey
	};
}

function verify(data,signature) {
	var pair = crypto.createDiffieHellman( 256 );
	var pairB = crypto.createDiffieHellman( 256 );
	pair.generateKeys();
	pairB.generateKeys();
	pairB.setPrivateKey( signature.hash );
	var pubb = pairB.getPublicKey();
	console.log( signature.publicKey.toString('hex') );
	var test = pairB.computeSecret( signature.publicKey );
	console.log('Outcome:',test.toString('hex'));
	console.log('Lock:',signature.secret.toString('hex'));
	return (test == signature.secret);
}


function buildKeys() {
	var pair = crypto.createDiffieHellman( 256  );
	var seed = crypto.randomBytes(32);
	var root=seed;

	for(i=0;i<255;i++) root = crypto.createHash('sha256').update(root).digest();
	var first = crypto.createHash('sha256').update(root).digest();
	pair.setPrivateKey( first );
	var publicKey = pair.getPublicKey();
console.log('public: ',publicKey);
	pair.setPrivateKey( root );
	publicKey = pair.getPublicKey();
console.log('public: ',publicKey);
	var badge = new Buffer.concat( [ first, publicKey ] );
	var address = bs58.encode( crypto.createHash('sha256').update(badge).digest() );

	var signature = sign( pair , 'hello world' );

	//ToDo, custom signature/verify routines

	// Decrypt this with my public key which is ... -- 

	console.log('Seed : ',seed.toString('hex'));
	console.log('Root : ',root.toString('hex'));
	console.log('First : ',first.toString('hex'));
	console.log('Public : ',pair.getPublicKey().toString('hex'));
	console.log('Badge : ', badge.toString('hex') );
	console.log('Address : ',address);
	console.log('Signature : ',signature);

	console.log(' -- Verify -- ');

	// We should only have signature, and message
	
	if (verify('hello world',signature)) {
		console.log('VERIFIED');
	} else {
		console.log('Unverified');
	}

}

//buildRoot();
buildKeys();
